<?php

return [
    'task' => [
        'status' => [
            1 => 'Новая задача',
            2 => 'Выполяется',
            3 => 'Остановлено',
            4 => 'Ошибка',
            5 => 'Обрабатывается...',
            6 => 'Выполнено',
        ],
    ],
];
