<?php

namespace App\Observers;

use App\Models\Sources\AbstractSource;
use App\Models\Sources\SourceHandlerFactory;

class SourceObserver
{
    /**
     * @var SourceHandlerFactory|null
     */
    private static ?SourceHandlerFactory $source_factory = null;


    public function __construct()
    {
        static::$source_factory ?: static::$source_factory = new SourceHandlerFactory();
    }

    public function retrieved(AbstractSource $source)
    {
        if ($source->type) {
            $source->setHandler(static::$source_factory->getHandler($source));
        }
    }
}
