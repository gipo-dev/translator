<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskStatus
{
    use HasFactory;

    public const STATUS_NEW = 1,
        STATUS_PROCESS = 2,
        STATUS_STOP = 3,
        STATUS_ERROR = 4,
        STATUS_FINISH = 6,
        STATUS_LOADING = 5;
}
