<?php

namespace App\Models;

use App\Jobs\Job;
use App\Jobs\TranslatorJob;
use App\Models\Sources\Source;
use App\Models\TaskResults\TaskResultFile;
use App\Models\TaskResults\TaskResultInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;
use Orchid\Attachment\Attachable;
use Orchid\Screen\AsMultiSource;

/**
 * Class TranslatorTask
 * @package App\Models
 *
 * @property Collection $sources
 */
class TranslatorTask extends Model
{
    use HasFactory;
    use AsMultiSource;
    use Attachable;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $attributes = ['status_id' => TaskStatus::STATUS_NEW];

    /**
     * @var string[]
     */
    protected $dates = ['started_at'];

    /**
     * @var TaskResultInterface
     */
    public TaskResultInterface $resultHandler;

    /**
     * TranslatorTask constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    protected static function booted()
    {
        parent::booted();

        static::retrieved(function (TranslatorTask $task) {
            $task->resultHandler = new TaskResultFile($task);
        });
    }

    /**
     * @return HasMany
     */
    public function sources(): HasMany
    {
        return $this->hasMany(Source::class);
    }

    /**
     * @return BelongsToMany
     */
    public function languages(): BelongsToMany
    {
        return $this->belongsToMany(Language::class);
    }

    /**
     * @return string
     */
    public function getSourcePath($absolute = true, $folder = 'source')
    {
        $path = 'tasks/' . $this->id . '/' . $folder;
        if ($absolute)
            return Storage::path($path);
        return $path;
    }

    /**
     * @return mixed
     */
    public function activeProxies()
    {
        return $this->hasMany(Proxy::class, 'task_id')->busy();
    }

    public function jobs()
    {
        return $this->belongsToMany(Job::class);
    }

    /**
     * @return string
     */
    public function getActivityDescription()
    {
        $active_proxies = $this->activeProxies;
        if (count($active_proxies) < 1)
            return 'Неактивно';
        $data = [
            'done_total' => 0,
            'total_tasks' => 0,
        ];
        $text = "Активны: {$active_proxies->count()} потоков";
        foreach ($active_proxies as $proxy) {
            $info = $proxy->data;
            $text .= '<br>' . ($info['done_count'] ?? '?') . '/' . ($info['total_tasks'] ?? '?') . ' avg: ' . ($info['avg_time'] ?? '?') . '/мин';
            $data['done_total'] += ($info['done_count'] ?? 0);
            $data['total_tasks'] += ($info['total_tasks'] ?? 0);
        }
        $text .= '<br>Запущено ' . $this->started_at->diffForHumans();
        $text .= '<br>Выполнено ~ ' . (now()->addSeconds(now()->diffInSeconds($this->started_at) / $data['done_total'] * ($data['total_tasks'] - $data['done_total']))->diffForHumans());
        return $text;
    }
}
