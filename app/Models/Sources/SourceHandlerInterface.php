<?php


namespace App\Models\Sources;


interface SourceHandlerInterface
{
    /**
     * SourceHandlerInterface constructor.
     * @param AbstractSource $source
     */
    public function __construct(AbstractSource $source);

    /**
     * @return array
     */
    public function getData(): array;
}
