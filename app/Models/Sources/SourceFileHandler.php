<?php


namespace App\Models\Sources;


class SourceFileHandler extends AbstractSourceHandler implements SourceHandlerInterface
{
    /**
     * @inheritDoc
     */
    public function getData(): array
    {
        $filepath = $this->source->data['path'];
        return [
            'title' => str_replace('_', ' ', pathinfo($filepath)['filename']),
            'content' => file_get_contents($filepath),
        ];
    }
}
