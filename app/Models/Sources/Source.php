<?php


namespace App\Models\Sources;


use Illuminate\Database\Eloquent\Factories\HasFactory;

class Source extends AbstractSource
{
    use HasFactory;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array|string[]
     */
    protected $casts = [
        'data' => 'array',
    ];
}
