<?php


namespace App\Models\Sources;


class AbstractSourceHandler
{
    /**
     * @var AbstractSource
     */
    protected AbstractSource $source;

    /**
     * SourceFileHandler constructor.
     * @param AbstractSource $source
     */
    public function __construct(AbstractSource $source)
    {
        $this->source = $source;
    }
}
