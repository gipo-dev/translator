<?php


namespace App\Models\Sources;


use Illuminate\Database\Eloquent\Model;

abstract class AbstractSource extends Model
{
    /**
     * @var SourceHandlerInterface
     */
    protected SourceHandlerInterface $handler;

    /**
     * AbstractSource constructor.
     * @param array $attributes
     * @throws \Exception
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = 'sources';
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->handler->getData();
    }

    /**
     * @param SourceHandlerInterface $handler
     */
    public function setHandler(SourceHandlerInterface $handler)
    {
        $this->handler = $handler;
    }
}
