<?php


namespace App\Models\Sources;


class SourceHandlerFactory
{
    /**
     * @param AbstractSource $source
     * @return SourceHandlerInterface
     * @throws \Exception
     */
    public function getHandler(AbstractSource $source): SourceHandlerInterface
    {
        $class_name = "App\Models\Sources\\Source" . ucfirst($source->type) . "Handler";
        if (!class_exists($class_name))
            throw new \Exception("Класс \"$class_name\" не существует");
        return new $class_name($source);
    }
}
