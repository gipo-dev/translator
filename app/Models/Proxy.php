<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsMultiSource;

/**
 * Class Proxy
 * @package App\Models
 *
 * @property Carbon $ping
 * @property string connectionString
 */
class Proxy extends Model
{
    use AsMultiSource;
    use HasFactory;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $attributes = ['description' => '', 'ping' => '2021-01-01 13:56:50'];

    /**
     * @var string[]
     */
    public $dates = ['created_at', 'updated_at', 'ping'];

    /**
     * @var string[]
     */
    protected $casts = ['data' => 'array'];

    /**
     * Только свободные прокси
     * @param Builder $builder
     */
    public function scopeVacant(Builder $builder)
    {
        $builder->where('ping', '<', now()->subMinutes(2))->orWhereNull('ping');
    }

    /**
     * Только свободные прокси
     * @param Builder $builder
     */
    public function scopeBusy(Builder $builder)
    {
        $builder->where('ping', '>', now()->subMinutes(2))->orWhereNull('ping');
    }

    /**
     * @return bool
     */
    public function isBusy()
    {
        if (!$this->ping)
            return false;
        return $this->ping->greaterThan(now()->subMinutes(2));
    }

    /**
     * @return bool
     */
    public function isLocal()
    {
        return $this->ip == '0.0.0.0';
    }

    /**
     * @param $task_id
     */
    public function ping($task_id, $data = [])
    {
        $this->update(['ping' => now(), 'task_id' => $task_id, 'data' => $data]);
    }

    /**
     * @return string
     */
    public function getNameAttibute()
    {
        return $this->ip . ':' . $this->port . '@' . $this->login . ':' . $this->password;
    }

    /**
     * @return string
     */
    public function getConnectionStringAttribute()
    {
        return 'http://' . $this->login . ':' . $this->password . '@' . $this->ip . ':' . $this->port;
    }
}
