<?php


namespace App\Models\TaskResults;


use Illuminate\Support\Facades\Storage;

class TaskResultFile extends AbstractTaskResult
{
    public function save(array $content, $folder = false): bool
    {
        $result_path = $this->task->getSourcePath(false, 'result') . '/';
        if ($folder)
            $result_path .= $folder . '/';
        $result_path .= str_replace(' ', '_', $content['title']) . '.txt';
        return Storage::put($result_path, $content['content']);
    }

    /**
     * @return string
     */
    private function getResultArchivePath($absolute = true)
    {
        $path = $this->task->getSourcePath(false, 'result.zip');
        if ($absolute)
            return public_path($path);
        return $path;
    }

    /**
     * @return bool
     */
    public function hasArchive(): bool
    {
        return file_exists($this->getResultArchivePath());
    }

    /**
     * @return string
     */
    public function getArchive(): string
    {
        return '/' . $this->getResultArchivePath(false);
    }
}
