<?php

namespace App\Models\TaskResults;

use App\Models\TranslatorTask;

interface TaskResultInterface
{
    /**
     * TaskResultInterface constructor.
     * @param TranslatorTask $task
     */
    public function __construct(TranslatorTask $task);

    /**
     * @param array $content
     * @param bool $folder
     * @return bool
     */
    public function save(array $content, $folder = false): bool;

    /**
     * @return bool
     */
    public function hasArchive(): bool;

    /**
     * @return string
     */
    public function getArchive(): string;
}
