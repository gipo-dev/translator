<?php


namespace App\Models\TaskResults;


use App\Models\TranslatorTask;

abstract class AbstractTaskResult implements TaskResultInterface
{
    protected TranslatorTask $task;

    public function __construct(TranslatorTask $task)
    {
        $this->task = $task;
    }
}
