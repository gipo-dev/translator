<?php

namespace App\Events;

use App\Models\TranslatorTask;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TaskSourceChanged
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public TranslatorTask $task;
    public $attachment_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(TranslatorTask $task, $attachment_id)
    {
        $this->task = $task;
        $this->attachment_id = $attachment_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
