<?php

namespace App\Services;

use App\Models\Proxy;
use Stichoza\GoogleTranslate\GoogleTranslate;

class Translate
{
    /**
     * @var GoogleTranslate
     */
    private GoogleTranslate $translator;

    /**
     * Translate constructor.
     * @param string $target_language
     * @param Proxy $proxy
     * @param string $source_lang
     */
    public function __construct(string $target_language, Proxy $proxy, $source_lang = 'ru')
    {
        $proxy_data = [
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko)'
            ],
            'timeout' => 10,
        ];
        if (!$proxy->isLocal())
            $proxy_data['proxy'] = $proxy->connectionString;

        $this->translator = new GoogleTranslate($target_language, $source_lang, $proxy_data);
    }

    /**
     * @param array $lines
     * @param bool $html
     * @return array|false
     */
    public function translate(array $lines, $html = false)
    {
        for ($i = 0; $i < 6; $i++) {
            try {
                $translated = [];
                $original = $lines;

                foreach ($lines as $key => $val) {
                    if ($html) {
                        $val = $this->replaceHtml($val);
                    }

                    $t = [];
                    $chunks = collect($this->wordChunk($val, 5000));
                    $start = microtime(true);
                    $chunks->chunk(7)->each(function ($chunk, $j) use (&$t, $start) {
                        foreach ($chunk as $k => $item) {
                            $translator = $this->translator;
                            $t[] = $translator->translate($item);
//                            $this->log->write('Перевел ' . $k . ' : ' . $j . ' - ' . (microtime(true) - $start) . 'сек');
                        }
                        usleep(1500 * 1000);
                    });

                    if ($html) {
                        $val = implode('', $t);

                        preg_match_all('/(<\/?.+?>)/', $original[$key], $tags);
                        $val = str_replace([
                            '===',
                            '{a}.',
                        ], [
                            '@',
                            '{a}',
                        ], $val);

                        $val = preg_replace('/(<?\s?[qQ]\s?>)/', '{a}', $val);

                        foreach ($tags[1] as $i => $tag) {
                            $pos = strpos($val, '{a}');
                            $tag = preg_replace('/(<\/)\s([a-zA-Z0-9]+>)/', '$1$2', $tag);
                            if ($pos !== false) {
                                $val = substr_replace($val, trim($tag), $pos, strlen('{a}'));
                            }
                        }

                        preg_match_all('~>\s*(https?://.+)\s*<~', $original[$key], $links);
                        foreach ($links[1] as $i => $tag) {
                            $pos = strpos($val, '<i>');
                            if ($pos !== false) {
                                $val = substr_replace($val, trim($tag), $pos, strlen('<i>'));
                            }
                        }
                        $translated[$key] = str_replace('‖', '=', $val);
//                        Storage::put('translated-html.txt', $val);
                    } else
                        $translated[$key] = implode('', $t);
                }

                return $translated;
            } catch (\Exception $exception) {
//                dd($exception);
                throw $exception;
            }
        }
        return false;
    }

    /**
     * @param string $str
     * @param int $len
     * @param string $end
     * @return array|string[]
     */
    private function wordChunk(string $str, int $len = 76, string $end = "||")
    {
        $arr = [];
        $res = '';

        foreach (mb_str_split($str) as $char) {
            if (strlen($res) + strlen($char) <= $len)
                $res .= $char;
            else {
                $last = strripos($res, ' ');
                $exp = substr($res, $last);
                $arr[] = substr($res, 0, $last) ?? '';
                $res = ($exp ?? '') . $char;
            }
        }
        $arr[] = $res;
        return $arr;
    }

    /**
     * @param string $text
     * @return string[]
     */
    private function replaceHtml(string $text)
    {
        $text = str_replace("&nbsp;", '', $text);
        $text = preg_replace(
            ['/(<\/?.+?>)/', '~<q>\s*(https?://.+)\s*<q>~'],
            [' <q> ', "<q><i><q>"],
            $text);
        return $text;
    }
}
