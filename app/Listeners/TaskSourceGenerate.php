<?php

namespace App\Listeners;

use App\Events\TaskSourceChanged;
use App\Models\Sources\Source;
use App\Models\TaskStatus;
use App\Models\TranslatorTask;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Orchid\Attachment\Models\Attachment;
use ZanySoft\Zip\Zip;

class TaskSourceGenerate implements ShouldQueue
{
    public $connection = 'database_long';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param TaskSourceChanged $event
     * @return void
     */
    public function handle(TaskSourceChanged $event)
    {
        $event->task->update(['status_id' => TaskStatus::STATUS_LOADING]);
        $source_path = $event->task->getSourcePath();
        $this->unzipSource($event, $source_path);
        $this->addSources($event->task, $source_path);
        $event->task->update(['status_id' => TaskStatus::STATUS_NEW]);
    }

    private function unzipSource(TaskSourceChanged $event, string $source_path)
    {
        /** @var Attachment $attachment */
        $attachment = Attachment::find($event->attachment_id)->first();
        $archive_path = public_path('storage/' . $attachment->physicalPath());
        $zip = new \ZipArchive();
        $zip->open($archive_path);
        $zip->extractTo($source_path);
        $attachment->delete();
    }

    private function addSources(TranslatorTask $task, string $source_path)
    {
        $files = collect(array_diff(scandir($source_path), array('.', '..')));
        $files
            ->chunk(100)
            ->each(function ($chunk) use ($task, $source_path) {
                $sources = [];
                foreach ($chunk as $file) {
                    $sources[] = [
                        'translator_task_id' => $task->id,
                        'type' => 'file',
                        'data' => json_encode([
                            'path' => $source_path . '/' . $file
                        ]),
                    ];
                }
                Source::insert($sources);
            });
    }
}
