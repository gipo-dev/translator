<?php

namespace App\Providers;

use App\Events\TaskSourceChanged;
use App\Listeners\TaskSourceGenerate;
use App\Models\Sources\AbstractSource;
use App\Models\Sources\Source;
use App\Observers\SourceObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        TaskSourceChanged::class => [
            TaskSourceGenerate::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Source::observe(SourceObserver::class);
    }
}
