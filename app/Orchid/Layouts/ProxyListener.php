<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Listener;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;

class ProxyListener extends Listener
{
    /**
     * List of field names for which values will be listened.
     *
     * @var string[]
     */
    protected $targets = [
        'proxies',
    ];

    /**
     * What screen method should be called
     * as a source for an asynchronous request.
     *
     * The name of the method must
     * begin with the prefix "async"
     *
     * @var string
     */
    protected $asyncMethod = 'asyncAddProxy';

    /**
     * @return Layout[]
     */
    protected function layouts(): array
    {
        return [
            Layout::table('proxies', [
                TD::make('description'),
                TD::make('ip'),
            ]),
        ];
    }
}
