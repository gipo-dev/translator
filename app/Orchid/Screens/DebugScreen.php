<?php

namespace App\Orchid\Screens;

use App\Models\Proxy;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Init;

class DebugScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'DebugScreen';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'DebugScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'rows' => Proxy::limit(1)->get(),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('rows', [
                TD::make('')->render(function () {
                    $serverMaxFileSize = Init::maxFileUpload(Init::MB);
                    phpinfo();
                    return $serverMaxFileSize;
                }),
            ]),
        ];
    }
}
