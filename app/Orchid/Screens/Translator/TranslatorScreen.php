<?php

namespace App\Orchid\Screens\Translator;

use App\Models\Proxy;
use App\Models\TranslatorTask;
use App\Orchid\Layouts\Cards\ProxyCard;
use App\Orchid\Layouts\ProxiesTable;
use App\Orchid\Layouts\ProxyListener;
use Orchid\Screen\Layouts\Card;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class TranslatorScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'TranslatorScreen';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'TranslatorScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'proxies' => Proxy::all(),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::columns([
                ProxyListener::class,
                ProxyListener::class,
            ]),
        ];
    }

    public function asyncAddProxy() {

    }
}
