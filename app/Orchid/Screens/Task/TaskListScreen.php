<?php

namespace App\Orchid\Screens\Task;

use App\Jobs\TranslatorJob;
use App\Jobs\ZipFileResult;
use App\Models\Proxy;
use App\Models\TaskStatus;
use App\Models\TranslatorTask;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class TaskListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Список задач';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'tasks' => TranslatorTask::with(['languages'])
                ->withCount(['sources'])
                ->paginate(20),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить задачу')
                ->route('platform.tasks.create')
                ->type(Color::PRIMARY()),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('tasks', [
                TD::make('name', 'Название'),
                TD::make('status', 'Статус')
                    ->render(function (TranslatorTask $task) {
                        return __('front.task.status.' . $task->status_id);
                    }),
                TD::make('languages', 'Языки перевода')
                    ->render(function (TranslatorTask $task) {
                        return $task->languages->pluck('code')->implode(', ');
                    }),
                TD::make('sources_count', 'Кол-во задач')->render(function (TranslatorTask $task) {
                    return $task->sources_count * $task->languages->count() . " ($task->sources_count файлов)";
                }),
                TD::make('', 'Активность')->render(function (TranslatorTask $task) {
                    return $task->getActivityDescription();
                })->popover('Активными считаются прокси, которые использовались менее 2 минут назад. Если задача остановлена - прокси перестанут быть "активными" в течение 2 минут. "avg" - сколько в среднем задач выполняется в минуту. Расчитано на основе последних 10 задач. Если значение avg слишком большое - скорее всего данный прокси заблокирован.'),
                TD::make('created_at', 'Дата создания')
                    ->render(function (TranslatorTask $task) {
                        return $task->created_at->diffForHumans();
                    }),
                TD::make()
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (TranslatorTask $task) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([
                                ModalToggle::make('Запустить')
                                    ->icon('control-play')
                                    ->type(Color::SUCCESS())
                                    ->parameters(['task' => $task->id])
                                    ->method('start')
                                    ->modal('modal_start')
                                    ->hidden(in_array($task->status_id, [TaskStatus::STATUS_PROCESS])),

                                Button::make('Остановить')
                                    ->icon('control-pause')
                                    ->type(Color::WARNING())
                                    ->parameters(['task' => $task->id])
                                    ->method('stop')
                                    ->hidden(!in_array($task->status_id, [TaskStatus::STATUS_PROCESS])),

                                Link::make('Редактировать')
                                    ->icon('pencil')
                                    ->type(Color::PRIMARY())
                                    ->route('platform.tasks.edit', $task),

                                ModalToggle::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->parameters(['task' => $task->id])
                                    ->method('delete')
                                    ->modal('modal_delete'),

                                Button::make('Сгенерировать архив')
                                    ->icon('modules')
                                    ->type(Color::LIGHT())
                                    ->parameters(['task' => $task->id])
                                    ->method('generateResult')
                                    ->hidden($task->resultHandler->hasArchive()),

                                Link::make('Скачать архив')
                                    ->icon('cloud-download')
                                    ->type(Color::LIGHT())
                                    ->href($task->resultHandler->getArchive())
                                    ->hidden(!$task->resultHandler->hasArchive()),
                            ]);
                    }),
            ]),

            Layout::modal('modal_delete', [])->title('Удалить задачу?')
                ->applyButton('Удалить')
                ->closeButton('Отмена'),

            Layout::modal('modal_start', [
                Layout::rows([
                    Select::make('proxies')
                        ->fromQuery(Proxy::vacant(), 'ip')
                        ->multiple()
                        ->required()
                        ->title('Выберите прокси для задачи (кол-во прокси = кол-во потоков)'),
                ]),
            ])->title('Запуск задачи')
                ->applyButton('Запустить')
                ->closeButton('Отмена'),
        ];
    }

    /**
     * @param Request $request
     * @param TranslatorTask $task
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        /** @var TranslatorTask $task */
        $task = TranslatorTask::find($request->task);
        foreach ($task->attachment as $item)
            $item->delete();
        Storage::deleteDirectory($task->getSourcePath(false));
        $task->delete();
        Toast::warning('Задача успешно удалена');
        return redirect()->back();
    }

    public function start(Request $request)
    {
        /** @var TranslatorTask $task */
        $task = TranslatorTask::find($request->task);
        $task->update(['status_id' => TaskStatus::STATUS_PROCESS, 'started_at' => now()]);
        $sources = $task->sources->pluck('id');
        $proxy = Proxy::find($request->proxies);
        $part = ceil($sources->count() / count($proxy));
        $sources = $sources->chunk($part);

        foreach ($sources as $i => $chunk) {
            /** @var Collection $chunk */
            $min = $chunk->min();
            $max = $chunk->max();
            TranslatorJob::dispatch($task, $proxy[$i], $min, $max);
        }
    }

    public function stop(Request $request)
    {
        $task = TranslatorTask::find($request->task);
        $task->update(['status_id' => TaskStatus::STATUS_STOP]);
    }

    public function generateResult(Request $request)
    {
        $task = TranslatorTask::find($request->task);
        $task->update(['status_id' => TaskStatus::STATUS_LOADING]);
        ZipFileResult::dispatch($task);
    }
}
