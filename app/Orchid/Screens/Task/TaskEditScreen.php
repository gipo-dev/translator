<?php

namespace App\Orchid\Screens\Task;

use App\Events\TaskSourceChanged;
use App\Models\Language;
use App\Models\TranslatorTask;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class TaskEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование задачи';

    private $task;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(TranslatorTask $task): array
    {
        $this->task = $task;

        if (!$task->exists)
            $this->name = 'Новая задача';

        return [
            'task' => $task,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->type(Color::SUCCESS())
                ->method('save'),
//            Button::make('Удалить')
//                ->type(Color::DANGER())
//                ->method('delete')
//                ->canSee($this->task->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('task.id')->hidden(),
                Input::make('task.name')
                    ->title('Название задачи')
                    ->required(),
                Input::make('task.status_id')
                    ->hidden(),
                Relation::make('task.languages.')
                    ->title('Языки перевода')
                    ->fromModel(Language::class, 'code')
                    ->multiple()
                    ->required(),
                Upload::make('source')
                    ->title('Архив с исходными файлами')
                    ->maxFiles(1)
                    ->maxFileSize(4096)
                    ->acceptedFiles('.zip'),
            ]),
        ];
    }

    /**
     * @param TranslatorTask $task
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(TranslatorTask $task, Request $request)
    {
        $task->fill(array_filter(collect($request->task)->except('languages')->toArray()))->save();
        $task->languages()->sync($request->task['languages']);
        if ($request->get('source'))
            event(new TaskSourceChanged($task, $request->get('source')));
        Toast::success('Задача успешно сохранена');
        return redirect()->route('platform.tasks.edit', $task);
    }
}
