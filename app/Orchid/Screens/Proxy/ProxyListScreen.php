<?php

namespace App\Orchid\Screens\Proxy;

use App\Http\Requests\ValidateProxyRequest;
use App\Models\Proxy;
use App\Services\Translate;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ProxyListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Список прокси';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '0.0.0.0 - это локальное проксирование, его удалять не нужно. При нажатии на кнопку "Тест" выполняется тестовый перевод. Если результат тестирования красный - значит либо прокси недоступен, либо переводчик заблокировал этот прокси. Обычно блок длится от несольких часов до суток';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'proxies' => Proxy::all(),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            ModalToggle::make('Добавить прокси')
                ->type(Color::PRIMARY())
                ->method('create')
                ->modal('modal_create'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('proxies', [
                TD::make('', 'Свободен')
                    ->render(function (Proxy $proxy) {
                        return CheckBox::make()->checked(!$proxy->isBusy());
                    })->width(50)->align('center'),
                TD::make('ip', 'IP'),
                TD::make('port', 'Порт'),
                TD::make('login', 'Логин'),
                TD::make('password', 'Пароль'),
                TD::make()->render(function (Proxy $proxy) {
                    return Button::make('Тест')
                        ->type(Color::INFO())
                        ->parameters(['proxy_id' => $proxy->id])
                        ->method('test')
                        ->hidden($proxy->isBusy());
                }),
                TD::make()->render(function (Proxy $proxy) {
                    return ModalToggle::make('Удалить')
                        ->type(Color::DANGER())
                        ->parameters(['proxy_id' => $proxy->id])
                        ->method('delete')
                        ->modal('modal_delete')
                        ->hidden($proxy->isLocal());
                }),
            ]),
            Layout::modal('modal_delete', [])->title('Удалить прокси?')
                ->applyButton('Удалить')
                ->closeButton('Отмена'),
            Layout::modal('modal_create', [
                Layout::rows([
                    Group::make([
                        Input::make('ip')
                            ->type('text')
                            ->mask('9{1,3}.9{1,3}.9{1,3}.9{1,3}')
                            ->title('IP')
                            ->required(),
                        Input::make('port')
                            ->title('Порт')
                            ->type('number')->required(),
                    ]),
                    Group::make([
                        Input::make('login')->title('Логин'),
                        Input::make('password')->title('Пароль'),
                    ]),
                ]),
            ])->title('Добавление прокси')
                ->applyButton('Создать')
                ->closeButton('Отмена'),
        ];
    }

    public function create(ValidateProxyRequest $request)
    {
        Proxy::create($request->all());
        Alert::success('Прокси успешно создан');
        return redirect()->back();
    }

    public function delete(Request $request)
    {
        Proxy::find($request->proxy_id)->delete();
        Alert::warning('Прокси успешно удален');
        return redirect()->back();
    }

    public function test(Request $request)
    {
        $proxy = Proxy::find($request->proxy_id);
        $translator = new Translate('en', $proxy);
        try {
            $result = $translator->translate(['test' => 'Привет мир']);
            Toast::success("Успешно: \n Привет мир -> " . $result['test']);
        } catch (\Exception $exception) {
            Toast::error($exception->getMessage());
            return redirect()->back();
        }
    }
}
