<?php

namespace App\Jobs;

use App\Models\Proxy;
use App\Models\Sources\AbstractSource;
use App\Models\TaskStatus;
use App\Models\TranslatorTask;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Translate;
use Illuminate\Support\Facades\Log;

class TranslatorJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;

    public $timeout = 259200;

    private TranslatorTask $task;
    private Proxy $proxy;
    private int $min;
    private int $max;
    /**
     * @var Translate[]
     */
    private $translators = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TranslatorTask $task, Proxy $proxy, int $min, int $max)
    {
        $this->onConnection('database_long');
        $this->task = $task;
        $this->proxy = $proxy;
        $this->min = $min;
        $this->max = $max;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $sources = $this->getSources();
        $languages = $this->task->languages->pluck('code');
        $this->setTranslators();
        $avg_items = [];
        $info = [
            'total_tasks' => $sources->count() * $languages->count(),
            'done_count' => 0,
            'avg_time' => 0,
        ];
        foreach ($sources as $i => $source) {
            $this->task->refresh();
            /** @var AbstractSource $source */
            foreach ($languages as $language) {
                if ($this->task->status_id == TaskStatus::STATUS_STOP) {
                    $this->fail(new \Exception('stopped by task'));
                    return;
                }

                $start_time = microtime(true);
                try {
                    $result = $this->translators[$language]
                        ->translate($source->getData(), true);
                    $this->task->resultHandler->save($result, $language);
                } catch (\Exception $exception) {
                    Log::error("{$this->task->name}:{$source->id} Ошибка перевода: {$exception->getMessage()}");
                }

                if (count($avg_items) > 10)
                    array_shift($avg_items);
                $avg_items[] = microtime(true) - $start_time;
                $info['done_count'] += 1;
                $info['avg_time'] = number_format(60 / (array_sum($avg_items) / count($avg_items)), 2);

                $this->proxy->ping($this->task->id, $info);
                usleep(1500 * 1000);

            }
            $source->delete();
        }

        if ($this->task->sources()->count() == 0)
            $this->task->update(['status_id' => TaskStatus::STATUS_FINISH]);
    }

    private function getSources()
    {
        return $this->task->sources()
            ->where('id', '>=', $this->min)
            ->where('id', '<=', $this->max)
            ->get();
    }

    private function setTranslators()
    {
        foreach ($this->task->languages as $language) {
            $this->translators[$language->code] = new Translate($language->code, $this->proxy);
        }
    }
}
