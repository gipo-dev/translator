<?php

namespace App\Jobs;

use App\Models\TaskStatus;
use App\Models\TranslatorTask;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ZipFileResult implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public TranslatorTask $task;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TranslatorTask $task)
    {
        $this->onConnection('database_long');
        $this->task = $task;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $res_path = public_path($this->task->getSourcePath(false, 'result'));
        $source_path = $this->task->getSourcePath(true, 'result');
        if (!file_exists($res_path))
            mkdir($res_path, 0777, true);
        $zip = new \ZipArchive();
        $zip->open($res_path . '/../result.zip', \ZipArchive::CREATE);
        $this->addDir($zip, $source_path, basename($source_path));
        $zip->close();
        $this->task->update(['status_id' => TaskStatus::STATUS_FINISH]);
    }

    public function addDir($zip, $location, $name)
    {
        $zip->addEmptyDir($name);
        $this->addDirDo($zip, $location, $name);
    }

    private function addDirDo($zip, $location, $name)
    {
        $name .= '/';
        $location .= '/';
        $dir = opendir($location);
        while ($file = readdir($dir)) {
            if ($file == '.' || $file == '..') continue;

            if (filetype($location . $file) == 'dir') {
                $this->addDir($zip, $location . $file, $name . $file);
            } else {
                $zip->addFile($location . $file, $name . $file);
            }
        }
    }
}
