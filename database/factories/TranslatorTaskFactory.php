<?php

namespace Database\Factories;

use App\Models\Model;
use App\Models\TaskStatus;
use App\Models\TranslatorTask;
use Illuminate\Database\Eloquent\Factories\Factory;

class TranslatorTaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TranslatorTask::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'status_id' => TaskStatus::STATUS_NEW,
        ];
    }
}
