<?php

namespace Database\Factories\Sources;

use App\Models\Sources\Source;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;

class SourceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Source::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type' => 'file',
            'data' => ['path' => $this->faker->filePath()],
        ];
    }
}
