<?php

namespace Database\Seeders;

use App\Models\Language;
use App\Models\Sources\Source;
use App\Models\TranslatorTask;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TranslatorTask::factory()->create()->each(function (TranslatorTask $task) {

            $task->sources()->saveMany(Source::factory()->count(50)->make());

            $task->languages()->saveMany(Language::all());

        });
    }
}
