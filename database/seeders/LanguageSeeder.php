<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    private $langs = [
        'en',
        'es',
        'de',
        'fr',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->langs as $lang) {
            Language::create([
                'code' => $lang,
            ]);
        }
    }
}
