<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslatorTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translator_tasks', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->smallInteger('status_id');
            $table->timestamps();
        });

        Schema::create('language_translator_task', function (Blueprint $table) {
            $table->id();
            $table->smallInteger('language_id');
            $table->bigInteger('translator_task_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translator_tasks');
        Schema::dropIfExists('language_translator_task');
    }
}
