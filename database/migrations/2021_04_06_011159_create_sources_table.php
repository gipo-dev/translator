<?php

use App\Models\Sources\SourceHandlerInterface;
use HaydenPierce\ClassFinder\ClassFinder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sources', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('translator_task_id');
            $table->enum('type', $this->getSourceTypes());
            $table->json('data')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sources');
    }

    /**
     * Возвращает допустимые типы источников
     *
     * @return array
     * @throws Exception
     */
    public function getSourceTypes(): array
    {
        return collect(ClassFinder::getClassesInNamespace('App\Models\Sources'))
            ->filter(function ($item) {
                return in_array(SourceHandlerInterface::class, class_implements($item));
            })->map(function ($item) {
                preg_match('/Source(\w+)Handler/', $item, $type);
                return strtolower($type[1] ?? '');
            })->toArray();
    }
}
